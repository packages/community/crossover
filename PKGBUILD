# Maintainer: Philip Müller <philm [at] manjaro [dot] org>
# Maintainer: Bernhard Landauer <bernhard@manjaro.org>
# Contributor: Erik Fleischer <erik [at] erlenweg [dot] de>
# Contributor: Cian Mc Govern <cian [at] cianmcgovern [dot] com>
# Contributor: Roland Singer <roland [at] manjaro [dot] org>
# Contributor: TheBenj <thebenj88 [at] gmail [dot] com>
# Contributor: Philipp 'TamCore' B. <philipp [at] tamcore [dot] eu>

pkgname=crossover
pkgver=25.0.0
pkgrel=1
_pkgdebrel=1
pkgdesc="Run Windows Programs on Linux"
arch=('x86_64')
url="https://www.codeweavers.com/crossover"
license=('LicenseRef-custom')
depends=(
    'python' 'desktop-file-utils' 'python-gobject' 'vte3' 'python-cairo'
    'lib32-glibc>=2.11' 'lib32-libice' 'lib32-libsm' 'lib32-libx11'
    'lib32-libxext' 'lib32-libxi' 'lib32-freetype2' 'lib32-libpng' 'lib32-zlib'
    'lib32-lcms2' 'lib32-libgl' 'lib32-libxcursor' 'lib32-libxrandr'
    'lib32-glu'
)
optdepends=(
    'unzip: required to install Guild Wars, automatic installer extraction'
    'lib32-alsa-lib: This is the preferred way to provide audio support to Windows applications.'
    "lib32-fontconfig: Makes it possible to find and use the system's TrueType fonts. This is strongly recommended for office-type applications."
    'lib32-libcups: Needed to print to printers managed by the CUPS system, which is most likely the case. It is strongly recommended for office-type applications.'
    'lib32-libdbus: This is needed for Windows applications to automatically detect CD-ROM and USB key insertion.'
#    'lib32-libexif: Some applications need to parse EXIF and read their data tags.'  ## AUR
    'lib32-libldap: Lets Windows applications access LDAP servers.'
    'lib32-libpulse: A featureful, general-purpose sound server (client library).'
    'lib32-gnutls: This is needed by applications that perform encryption or check online certificates.'
    'lib32-gstreamer: This is needed by some games and multimedia applications.'
    'lib32-gst-plugins-base: This is needed by some games and multimedia applications.'
    'lib32-libxcomposite: This is needed for most CAD-like applications and some games.'
    "lib32-libxinerama: This is needed if your display spans multiple screens. If your computer has a single screen then you don't need it."
    'lib32-libxml2: This library makes it possible for Windows applications read and write XML files.'
    'lib32-libxslt: This library lets Windows applications perform queries and transformations on XML files.' 
    'lib32-libxxf86vm: This is needed to let games perform some gamma adjustments (essentially to adjust the brightness).'
    'lib32-libxxf86dga: X11 Direct Graphics Access extension library.'
    'lib32-mpg123: Needed by some Windows applications to play MP3 files.'
    'lib32-nss-mdns: host name resolution via mDNS'
    'lib32-openal: Provides audio support to Windows applications.'
    'lib32-openssl: This library provides support for secure Internet communication.'
    'lib32-v4l-utils: Lets Windows applications access video devices.'
)
install="${pkgname}.install"
source=("https://media.codeweavers.com/pub/${pkgname}/cxlinux/demo/${pkgname}_${pkgver}-${_pkgdebrel}.deb")
sha256sums=('5a4bb4a8d5efd0df70830a79dbd05153727111ade265ba53d6d1b8a8acd9581b')

package() {
    ar -p "${pkgname}_${pkgver}-${_pkgdebrel}.deb" data.tar.xz | bsdtar Jxf - -C "${pkgdir}" || return 1

    sed -e 's!;;"MenuRoot" = ""!"MenuRoot" = "Windows Games"!' \
        -e 's!;;"MenuStrip" = ""!"MenuStrip" = "1"!' \
        -i "${pkgdir}/opt/cxoffice/share/${pkgname}/bottle_data/cxbottle.conf"

    install -d "${pkgdir}/usr/bin"
    ln -s "/opt/cxoffice/bin/${pkgname}" "${pkgdir}/usr/bin/${pkgname}"
    ln -s /opt/cxoffice/bin/cxsetup "${pkgdir}/usr/bin/cxsetup"

    install -d "${pkgdir}/etc/profile.d"
    echo '[ -d /opt/cxoffice/bin ] && export PATH="${PATH}:/opt/cxoffice/bin"' > "${pkgdir}/etc/profile.d/cxoffice.sh"
    echo '[ -d /opt/cxoffice/bin ] && setenv PATH ${PATH}:/opt/cxoffice/bin' > "${pkgdir}/etc/profile.d/cxoffice.csh"
    chmod 755 "${pkgdir}/etc/profile.d/cxoffice.sh" "${pkgdir}/etc/profile.d/cxoffice.csh"

    # Fix Auto update error
    install -m 644 -D "${pkgdir}/opt/cxoffice/share/${pkgname}/data/cxoffice.conf" "${pkgdir}/opt/cxoffice/etc/cxoffice.conf"
    sed -e 's!;;"PrivateShortcutDirs" = ""!"PrivateShortcutDirs" = "${HOME}/bin:${CX_ROOT}/bin"!' \
        -e 's!;;"PrivateLinuxNSPluginDirs" = ""!"PrivateLinuxNSPluginDirs" = "${MOZ_PLUGIN_PATH}"!' \
        -e 's!;;"PrivateLinux64NSPluginDirs" = ""!"PrivateLinux64NSPluginDirs" = "${MOZ_PLUGIN_PATH}"!' \
        -e 's!;;"ProductPackage" = ""!"ProductPackage" = "Converted from .deb to pacman."!' \
        -i "${pkgdir}/opt/cxoffice/etc/cxoffice.conf"

    # place license in correct directory
    install -d "${pkgdir}/usr/share/licenses/${pkgname}/"
    cat "${pkgdir}/usr/share/doc/${pkgname}/license.txt.gz" | gzip -d > "${pkgdir}/usr/share/licenses/${pkgname}/license.txt"
    ln -s "/usr/share/doc/${pkgname}/copyright" "${pkgdir}/usr/share/licenses/${pkgname}/"

    # Remove Lintian
    rm -rf "${pkgdir}/usr/share/lintian/"
}
